---
layout: "page"
title: "Project Development Experience"
permalink: /devExp/
---

#  Development Experience


1.	**What did you learn about team or large project development? What will you start doing, keep doing, or stop doing next time?**

    After this teamwork, we first learned how to use Android studio to complete a large project and connect to Android studio through GitLab. Second, when we started with a large project, we learned to set up the project’s feature and user story first. According to the features we set up to implement the project functions. In addition, in the process of writing code, we understand and set up the SQL database, and follow the SOLID principle to write the code, reduce the appearance of smell, and then complete the UI design and writing How to do test (integration test, unit test and system test), and modify the code through the results of the test to make our code more perfect. The discussion atmosphere between the groups is also very harmonious. In the process of discussing the project, we will actively solve the problems when we encounter them. This has improved our self-learning ability and communication ability, and we have also learned how to get the results we want through discussion. (Complete the preparation of the code to realize the feature).

    Before the next large-scale project starts, we should first list the timetable and arrange the time reasonably to achieve certain work within the specified time. For example, complete the infrastructure in iteration 1, and implement the main features and do a unit test in iteration 2. And we also need to carry out work assignments in advance to solve the difficulties encountered in the work during the discussion. Secondly, we should consult some materials to improve the features of our project and understand how to implement these features. We think that the completion of this project was very successful, because the features we set up at that time were all perfectly implemented. We will continue to allocate work to implement the features, like everyone is responsible for part of the writing, one person completes the UI design, one People complete the database, then we will discuss in group and integrate our work. When we encounter problems, we will discuss them in the group, and we will solve the problem together. This approach improves our efficiency and guarantees the quality of the code. But in the next large-scale project, we will not only write code according to the features we set at the beginning, because some of the original features cannot be implemented, such as sharing schedules, we need to add features to replace that one according to the needs of large-scale projects, such as we did not add the feature of register student at the beginning, we added this feature later according to the UI design. Next time if we encounter a feature that cannot implemented, we will not waste too much time on it and will add new features to supplement our project.


2.	**What took the most time? the least? any surprises?**

    When working on setting up the database, the thing that took the most time was figuring out how to have all tables populating for the same database. We had all tables created but we were being successful in making only one table appear at a time. We were supposed to have this ready by the end of iteration 2 and we expected we would, but this turned out to be a little tricky and cost us a few hours. The least challenging would have been setting up the database from scratch where it had one table working.
    Apart from having difficulty with making all tables populate, we didn’t find any other surprises when setting up the database.


3. **This project has 3-tier architecture: Presentation Layers, Logic Layers, and data Layers (Persistence). Except those, the project also has some Layers not be forced on this time: Object Layers and Sevices Layers(application).**

    1.	For  Presentation Layers: 
        MainActivity.java: the main page supports activities in which student can check self-information, logout, update Student self-information. LoginActivity.java: work on the first page in the App. It supports activities: register Student, log in. CourseActivity.java: It supports activities: handle the Add course and show courses information on the Course Page. ScheduleActivity.java: supports activities: list the student’s Schedule and prove operations(go add courses, delete courses, delete current Schedule).

    2.	For  Logic Layers: 
        The three major java files AccessCourse, AccessStudent, and AccessSchedule access Data Layers for each type of object.  They prove the method on how to contain logic, when to load data from Data Layers and how to input data into each object persistence in Data Layers.
        The Validator.java makes sure that the student entries such as name and student ids are being added correctly, making sure that invalid characters aren't being added.
        The ValidatorCourse takes care of validation for the course where it checks for time conflict in courses and making sure a course isn't added twice.
    3.	For  Data Layers:
        There are three interfaces to isolation the three different objects persistence to implements methods to access SQLite Databases. 
        In SQLite Databases, we have "Course" "Schedule" "Student"  tables to help store data and use tablenamePersistence to work on them.
        We use an interface “IDatabase” as the superclass to three interfaces. Then we don't have interfaces that have methods that aren't needed.

    4.	For  Object Layers: 
        It has Course, Student, Schedule three objects. And Schedule is Composition by Course and Student.

    5.	For  Sevices Layers:
        Services.java class creates the objects Persistence before when the app starts.




