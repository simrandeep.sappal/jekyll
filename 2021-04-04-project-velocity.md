---
layout: "page"
title: "Project Velocity"
permalink: /velocity/
---

# Project Velocity for Group 9


![Course Scheduler Iterations Velocity](/_site/assets/Course_Schedule_Velocity.jpg)

If you have any trouble accessing the image please click the link here: [Velocity Graph](https://gitlab.com/simrandeep.sappal/jekyll/-/blob/master/2021-04-04-project-velocity.md)




# Summary of Project Velocity

The team had better estimates as we progressed through our project. During the initial iteration of the project, we estimated higher and we were only able todeliver half of it.
During Iteration 2, we had multiple bugs and technical debt that we had to resolve. Therefore the features that were estimated for iteration 2 were not met.
In the development process of Iteration 3 we were able to resolve almost all of the technical debts and bugs. We were also able to add new features to the app that we had estimated.
Due to the limited time constraints, we were not able to deliver some other features such as representing the schedule in a calendar format and thus any other features relating to it were moved to future development.
Througout the project, our estimation have gotten better as can be observed in the graph below.

