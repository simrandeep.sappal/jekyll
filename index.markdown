---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

# Welcome to Course Scheduler

Course Scheduler is an exiting android application developed by the crazy yet awesome students in Group 9 from COMP 3350. 

As university students we are often looking forward to the day of course registration. We sit down on our computer and as the clock hits the registration opening time, the website slows down and our anxiety continues to grow. Hoping that we will make it in time to register the course we want. But sometimes the courses we thought to register for have overlapping time slots and this causes both the courses to be unregistered. In this panic we loose more time and eventually loose the opportunity to be part of those courses.

Course Scheduler app allows users to simulate that experience without added pressure of anxiety and stress. This allows users to create their own schedule for courses before hand and verify if the courses that they plan to register for are not overlapping and fits according to the their needs.

We hope that you are as excited to be part of our journey just as we are. The app is comming this summer on all android devices !

We encourage you to visit our source code for CourseScheduler app in GitLab to explore how we brought this exiting new project to your hands:
[Gitlab](https://code.cs.umanitoba.ca/3350-winter-2021-a03/coursescheduler-comp3350-a03-group9)

